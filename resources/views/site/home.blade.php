<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/app.css">

    <title>Hello, world!</title>
</head>
<body>
<div id="app">

    <div class="container">
        <div class="contact-top text-right">
            <a class="phone" href="tel:88002011791">8-800-201-17-91</a><br>
            <a class="email" href="mailto:eszdp22@gmail.com">eszdp22@gmail.com</a>
        </div>

        <div class="company-name">
            <div class="row">
                <div class="col-3 col-lg-4">
                    <img src="img/logo-lg.png" alt="Единая Служба Железнодорожных Перевозок" class="img-fluid">
                </div>
                <div class="col my-auto white-grad-bg py-2 py-lg-5">
                    <h1 class="company-name__first-part">Единая Служба</h1>
                    <h3 class="company-name__second_part">Железнодорожных Перевозок</h3>
                </div>
            </div>
        </div>

        <div class="info-block mt-4 mt-lg-5">
            <h2 class="info-block_header text-center">Заказать расчет</h2>
            <calculator></calculator>
        </div>
    </div>
    <div class="vagon-line mt-4 mb-4 my-lg-5"></div>
    <div class="container">
        <div class="services text-center">
            <h2 class="header-lev2 services_legend"><span class="header-lev2__bg">Наши услуги</span></h2>
            <div class="row">
                <div class="col-6">
                    <img src="img/gruzootpravitel.png" class="img-fluid" alt="">
                    <h3 class="services_service text-center mt-2 mb-3 mb-lg-4">УСЛУГИ ГРУЗООТПРАВИТЕЛЯ</h3>
                </div>
                <div class="col">
                    <img src="img/pogruzka_vagonov.png" class="img-fluid" alt="">
                    <h3 class="services_service text-center mt-2 mb-3 mb-lg-4">ПОГРУЗКА<br>ВАГОНОВ</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <img src="img/zakazat_vagon.png" class="img-fluid" alt="">
                    <h3 class="services_service text-center mt-2">ЗАКАЗАТЬ<br>ВАГОН</h3>
                </div>
                <div class="col">
                    <img src="img/pogruzka_zerna.png" class="img-fluid" alt="">
                    <h3 class="services_service text-center mt-2">ПОГРУЗКА ЗЕРНА НА ОСОБЫХ УСЛОВИЯХ</h3>
                </div>
            </div>
        </div>

        <div class="info-block mt-4 mt-lg-5">
            <h2 class="info-block_header info-block_header__green text-center mb-lg-5">Заказать вагон</h2>
            <div class="row text-center">
                <div class="col">
                    <img src="img/vagon/platforma.png" alt="Платформа" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name mb-lg-4">Платформа</h3>
                    <img src="img/vagon/poluvagon.png" alt="Полувагон" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name vagon-name__red mb-lg-4">Полувагон</h3>
                    <img src="img/vagon/hopper.png" alt="Хоппер" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name vagon-name__green">Хоппер</h3>
                </div>
                <div class="col">
                    <img src="img/vagon/kritie.png" alt="Крытые" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name vagon-name__dark-green mb-lg-4">Крытые</h3>
                    <img src="img/vagon/container.png" alt="Контейнер" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name vagon-name__red mb-lg-4">Контейнер</h3>
                    <img src="img/vagon/cisterna.png" alt="Цистерна" class="img-fluid">
                    <h3 class="mt-2 mb-3 vagon-name">Цистерна</h3>
                </div>
            </div>
        </div>

        <h3 class="text-center mt-4 header-lev2">Звоните</h3>
        <div class="text-center white-grad-bg py-2 py-lg-4">
            <a href="tel:88002011791" class="phone phone__size-big">8-800-201-17-91</a><br>
            <a href="mailto:eszdp22@gmail.com" class="email email__size-big">eszdp22@gmail.com</a>
        </div>
    </div>
    <div class="vagon-line mt-4 mb-4"></div>

</div>

<script src="js/app.js"></script>

</body>
</html>