<?php

namespace App\Http\Requests\Calculator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CalculateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departureStation' => ['required', Rule::exists('stations', 'id')],
            'destinationStation' => ['required', Rule::exists('stations', 'id')],
            'carriage' => ['required', Rule::exists('carriages', 'id')],
            'cargo' => ['required', Rule::exists('cargoes', 'id')],
        ];
    }
}
