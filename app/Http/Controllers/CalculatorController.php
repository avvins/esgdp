<?php

namespace App\Http\Controllers;

use App\Http\Requests\Calculator\CalculateRequest;
use App\Http\Resources\CargoSelectOptions;
use App\Http\Resources\CarriageSelectOptions;
use App\Http\Resources\StationSelectOptions;
use App\Models\Cargo;
use App\Models\Carriage;
use App\Models\Station;

class CalculatorController extends Controller
{
    public function stations(string $query)
    {
        $stations = Station::where('code', 'LIKE', '%' . $query . '%')
            ->orWhere('name', 'LIKE', '%' . $query . '%')
            ->take(10)
            ->get();
        return StationSelectOptions::collection($stations);
    }

    public function carriages(string $query = '')
    {
        $carriages = Carriage::where('name', 'LIKE', '%' . $query . '%')
            ->take(10)
            ->get();
        return CarriageSelectOptions::collection($carriages);
    }

    public function cargoes(string $query)
    {
        $cargoes = Cargo::where('code', 'LIKE', '%' . $query . '%')
            ->orWhere('name', 'LIKE', '%' . $query . '%')
            ->take(10)
            ->get();
        return CargoSelectOptions::collection($cargoes);
    }

    public function calculate(CalculateRequest $request)
    {
        $departureStationId = $request->get('departureStation');
        $destinationStationId = $request->get('destinationStation');

        $processed = [$departureStationId];

        $station = Station::find($departureStationId);

        $costs = [$destinationStationId => INF];
        $distances = $station->distances();
        foreach ($distances as $node) {
            $costs[$station->id !== $node->station_out_id ? $node->station_out_id : $node->station_in_id] = $node->distance;
        }

        $keyNode = $this->lowestCostNode($costs, $processed);
        while ($keyNode !== null) {
            $cost = $costs[$keyNode];
            $neighbors = Station::find($keyNode)->distances();
            foreach ($neighbors as $neighbor) {
                $newCost = $cost + $neighbor->distance;
                $interStationId = $keyNode !== $neighbor->station_out_id ? $neighbor->station_out_id : $neighbor->station_in_id;
                if (!isset($costs[$interStationId]) || $costs[$interStationId] > $newCost)
                    $costs[$interStationId] = $newCost;
            }
            $processed[] = $keyNode;
            $keyNode = $this->lowestCostNode($costs, $processed);
            if ($keyNode == $destinationStationId) {
                dd($costs);
            }
        }

        dd($costs);
        return ['asdf' => 'asdf'];
    }

    private function lowestCostNode($costs, $processed)
    {
        $lowestCost = INF;
        $lowestCostKey = null;
        foreach ($costs as $key => $distance) {
            if ($distance < $lowestCost && !in_array($key, $processed)) {
                $lowestCost = $distance;
                $lowestCostKey = $key;
            }
        }
        return $lowestCostKey;
    }
}
