<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{
    public function stationOut()
    {
        return $this->belongsTo(Station::class, 'station_out_id');
    }

    public function stationIn()
    {
        return $this->belongsTo(Station::class, 'station_in_id');
    }
}
