<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
//    public function distances()
//    {
//        return $this->hasMany(Distance::class, 'station_out_id', 'id');
//    }
    public function distancesOut() {
        return $this->hasMany(Distance::class, 'station_out_id', 'id');
    }

    public function distancesIn() {
        return $this->hasMany(Distance::class, 'station_in_id', 'id');
    }

    public function distances() {
        return $this->distancesOut->merge($this->distancesIn);
    }
}
