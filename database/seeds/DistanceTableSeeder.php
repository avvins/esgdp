<?php

use App\Models\Station;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use League\Csv\Reader;

class DistanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(database_path('seeds/data/distance.csv'), 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();
        foreach ($records as $record) {
            try {
                $station1 = Station::where('code', $record['codeOut'])->firstOrFail();
                $station2 = Station::where('code', $record['codeIn'])->firstOrFail();

                DB::table('distances')->insert([
                    'station_out_id' => $record['codeOut'] < $record['codeIn'] ? $station1->id : $station2->id,
                    'station_in_id' => $record['codeOut'] >= $record['codeIn'] ? $station1->id : $station2->id,
                    'distance' => $record['distance']
                ]);
            } catch (QueryException $e) {
                echo 'Дублировние расстояния: ' . $record['codeOut'] . ' - ' . $record['codeIn'] . PHP_EOL;
            } catch (ModelNotFoundException $e) {
                echo 'Какой то из станций нет в базе: ' . $record['codeOut'] . ' или ' . $record['codeIn'] . PHP_EOL;
            } catch (Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
    }
}
