<?php

use Illuminate\Database\Seeder;

class CarriagesTableSeeder extends Seeder
{
    const carriageTypes = [
        'Крытый вагон',
        'Полувагон',
        'Рефрижератор',
        'Хоппер',
        'Цистерна',
        'Платформа'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::carriageTypes as $carriageType) {
            DB::table('carriages')->insert([
                'name' => $carriageType
            ]);
        }
    }
}
