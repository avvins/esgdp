<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;

class CargoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath( database_path('seeds/data/etsng.csv'), 'r');
        $csv->setDelimiter('|');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();
        foreach ($records as $record) {
            DB::table('cargoes')->insert([
                'code' => $record['code'],
                'name' => $record['name'],
            ]);
        }
    }
}
