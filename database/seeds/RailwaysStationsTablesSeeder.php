<?php

use App\Models\Railway;
use App\Models\Station;
use Illuminate\Database\Seeder;
use League\Csv\Reader;

class RailwaysStationsTablesSeeder extends Seeder
{
    const railways = [
        'Восточно-Сибирская железная дорога',
        'Горьковская железная дорога',
        'Дальневосточная железная дорога',
        'Забайкальская железная дорога',
        'Западно-Сибирская железная дорога',
        'Калининградская железная дорога',
        'Красноярская железная дорога',
        'Куйбышевская железная дорога',
        'Московская железная дорога',
        'Октябрьская железная дорога',
        'Приволжская железная дорога',
        'Свердловская железная дорога',
        'Северная железная дорога',
        'Северо-Кавказская железная дорога',
        'Юго-Восточная железная дорога',
        'Южно-Уральская железная дорога',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::railways as $railwayName) {
            $railway = Railway::create(['name' => $railwayName]);
            $csv = Reader::createFromPath( database_path('seeds/data/stations/' . explode(' ', $railway->name)[0] . '.csv'), 'r');
            $csv->setDelimiter(';');
            $csv->setHeaderOffset(0);
            $records = $csv->getRecords();
            foreach ($records as $record) {
                $railway->stations()->save(factory(Station::class)->make(['code' => $record['code'], 'name' => $record['name']]));
            }
        }
    }
}
