<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('station_out_id')->unsigned()->index();
            $table->bigInteger('station_in_id')->unsigned()->index();
            $table->integer('distance');
            $table->timestamps();

            $table->foreign('station_out_id')->references('id')->on('stations');
            $table->foreign('station_in_id')->references('id')->on('stations');

            $table->unique(['station_out_id', 'station_in_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distances');
    }
}
